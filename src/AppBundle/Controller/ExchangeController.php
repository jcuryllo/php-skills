<?php

namespace AppBundle\Controller;

use Application\Exception\CurrencyPairNotSupported;
use Application\Exception\IncorrectInputData;
use Money\Currency;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeController extends Controller
{
    /**
     * @Route("/exchange", name="exchange")
     */
    public function indexAction(Request $request)
    {
        $calculationForm = '';

        if ($request->getMethod() === 'POST') {
            $calculationForm = $request->request->get('calculationForm');
            $rate = 0.0;
            $dataArray = array();

            try {
                $dataArray = $this->get('exchange_extractor.rules_extractor')->getDataFromString($calculationForm['convert']);
            } catch (IncorrectInputData $e) {
                $this->addFlash(
                    'warning',
                    'Incorrect or not supported input data!'
                );
            } catch (CurrencyPairNotSupported $e) {
                $this->addFlash(
                    'warning',
                    'Unsupported pair of currencies!'
                );
            }

            foreach ($dataArray as &$data) {
                try {
                    $currencyIn = new Currency(mb_strtoupper($data['initialCurrency']));
                    $currencyOut = new Currency(mb_strtoupper($data['secondCurrency']));
                    $data['rate'] = $this->get('exchange_rate.yahoo_provider')->fetch($currencyIn, $currencyOut);
                    $data['amountConverted'] = $data['amount'] * $data['rate'];
                } catch (CurrencyPairNotSupported $e) {
                    $this->addFlash(
                        'warning',
                        'Unsupported pair of currencies!'
                    );
                } catch (IncorrectInputData $e) {
                    $data = [
                        'correct' => false,
                        'initialCurrency' => null,
                        'secondCurrency' => null,
                        'amount' => 0.0,
                        'rate' => 0.0,
                        'amountConverted' => 0.0
                    ];

                    $this->addFlash(
                        'warning',
                        'Incorrect or not supported input data!'
                    );
                }
            }
        }

        return $this->render(
            'exchange/index.html.twig', ['dataArray' => $dataArray ?? null]);
    }

}
