<?php

namespace Application\ExchangeRate;


use Application\Exception\CurrencyPairNotSupported;
use Money\Currency;

class YahooExchangeRate implements ExchangeRateProvider
{

    public function fetch(Currency $currencyIn, Currency $currencyOut)
    {

        if ($currencyIn->getCode() === 'GBP' && $currencyOut->getCode() === 'PLN') {
            return $this->getResult($currencyIn, $currencyOut);
        }

        if ($currencyIn->getCode() === 'GBP' && $currencyOut->getCode() === 'EUR') {
            return $this->getResult($currencyIn, $currencyOut);
        }

        if ($currencyIn->getCode() === 'PLN' && $currencyOut->getCode() === 'EUR') {
            return $this->getResult($currencyIn, $currencyOut);
        }

        if ($currencyIn->getCode() === 'PLN' && $currencyOut->getCode() === 'GBP') {
            return $this->getResult($currencyIn, $currencyOut);
        }

        if ($currencyIn->getCode() === 'EUR' && $currencyOut->getCode() === 'PLN') {
            return $this->getResult($currencyIn, $currencyOut);
        }

        throw new CurrencyPairNotSupported();
    }

    protected function getResult(Currency $currencyIn, Currency $currencyOut)
    {
        $url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in(%22" . mb_strtolower($currencyIn->getCode()) . mb_strtolower($currencyOut->getCode()) . "%22)&env=store://datatables.org/alltableswithkeys";
        $xml = simplexml_load_file($url);
        $rate = $xml->results->rate[0]->Rate;

        return floatval($rate);
    }
}