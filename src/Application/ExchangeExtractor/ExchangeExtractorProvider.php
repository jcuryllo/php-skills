<?php

namespace Application\ExchangeExtractor;

/**
 * Interface ExchangeExtractorProvider
 */
interface ExchangeExtractorProvider
{
    const CURRENCIES = [
        'pln',
        'gbp',
        'eur'
    ];

    const CURRENCIES_EXT = [
        'pln' => [
            0 => 'zł',
            1 => [
                'pln',
                'złoty',
                'zloty',
                'zlotys',
                'złotówek',
                'złotówka',
                'zł',
            ],
            2 => 'zloty'
        ],
        'gbp' => [
            0 => '£',
            1 => [
                'gbp',
                'pounds',
                'pound',
                'funtów',
                'funty',
                'funt',
                'libras',
                'libra'
            ],
            2 => 'pound'
        ],
        'eur' => [
            0 => '€',
            1 => [
                'euros',
                'euro',
                'eur'
            ],
            2 => 'euro'
        ]
    ];

    public function getDataFromString($string);
}
