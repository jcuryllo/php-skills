<?php

namespace Application\ExchangeExtractor\Rule;


use Application\Exception\IncorrectInputData;
use Application\ExchangeExtractor\ExchangeExtractorProvider;
use Application\ExchangeExtractor\RulesExtractor;

class NaturalSimpleRule implements ExchangeRule
{

    public static function extract($string)
    {
        $ruleExtractor = new RulesExtractor();

        // check if we have known words and one number
        // e.x. how much is 10050 pounds in euro
        if (preg_match('/([0-9\.]+)/u', $string, $matches, PREG_OFFSET_CAPTURE)) {
            $result['amount'] = (float)$matches[1][0];

            $count = 0;

            // search only in the part after first number
            $explodedString = explode(" ", substr($string, $matches[1][1]));
            foreach ($explodedString as $exploded) {
                foreach (ExchangeExtractorProvider::CURRENCIES_EXT as $w) {
                    foreach ($w[1] as $word) {
                        if (strpos($exploded, $word) !== false) {
                            if ($count == 0) {
                                $result['initialCurrency'] = $word;
                                $count++;
                            }
                            if ($count == 1 && $ruleExtractor->normalize($word) !== $ruleExtractor->normalize($result['initialCurrency'])) {
                                $result['secondCurrency'] = $word;
                                $count++;
                                break 2;
                            }
                        }
                    }
                }
            }

            if ($count < 2) {
                return null;
            }

            return [$result];
        }
        return null;
    }
}