<?php

namespace Application\ExchangeExtractor\Rule;


class SimpleRuleReversed implements ExchangeRule
{

    public static function extract($string)
    {
        // e.x. convert 500 usd to pln
        if (preg_match('/\s*(\p{L}+)\s+([0-9\.]+)\s+(\p{L}+)\s+(\p{L}+)\s+(\p{L}+)\s*/u', $string, $matches)) {
            $result['initialCurrency'] = $matches[3];
            $result['secondCurrency'] = $matches[5];
            $result['amount'] = $matches[2];

            return [$result];
        }
        return null;
    }
}