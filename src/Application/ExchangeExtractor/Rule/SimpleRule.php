<?php

namespace Application\ExchangeExtractor\Rule;


class SimpleRule implements ExchangeRule
{

    public static function extract($string)
    {
        // e.x. convert eur 500 to pln
        if (preg_match('/\s*(\p{L}+)\s+(\p{L}+)\s+([0-9\.]+)\s+(\p{L}+)\s+(\p{L}+)\s*/u', $string, $matches)) {
            $result['initialCurrency'] = $matches[2];
            $result['secondCurrency'] = $matches[5];
            $result['amount'] = $matches[3];
            return [$result];
        }
        return null;
    }
}