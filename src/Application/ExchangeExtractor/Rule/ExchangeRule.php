<?php

namespace Application\ExchangeExtractor\Rule;


interface ExchangeRule
{
    public static function extract($string);
}