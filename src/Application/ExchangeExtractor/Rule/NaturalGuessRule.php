<?php

namespace Application\ExchangeExtractor\Rule;


use Application\Exception\IncorrectInputData;
use Application\ExchangeExtractor\ExchangeExtractorProvider;

class NaturalGuessRule implements ExchangeRule
{

    public static function extract($string)
    {
        // check if we have known words and one number
        // e.x. how much euro is 10050 pounds
        if (preg_match('/([0-9\.]+)/u', $string, $matches, PREG_OFFSET_CAPTURE)) {
            $result['amount'] = $matches[1][0];

            // search for the first currency only before the number
            $explodedString = explode(" ", substr($string, 0, $matches[1][1]));
            foreach ($explodedString as $exploded) {
                foreach (ExchangeExtractorProvider::CURRENCIES_EXT as $w) {
                    foreach ($w[1] as $word) {
                        if (strpos($exploded, $word) !== false) {
                            $result['secondCurrency'] = $word;
                            break 3;
                        }
                    }
                }
            }

            // search for the second currency only after the number
            $explodedString = explode(" ", substr($string, $matches[1][1]));
            foreach ($explodedString as $exploded) {
                foreach (ExchangeExtractorProvider::CURRENCIES_EXT as $w) {
                    foreach ($w[1] as $word) {
                        if (strpos($exploded, $word) !== false) {
                            $result['initialCurrency'] = $word;
                            break 3;
                        }
                    }
                }
            }

            if (!isset($result['initialCurrency'], $result['secondCurrency'])) {
                return null;
            }

            return [$result];
        }
        return null;
    }
}