<?php

namespace Application\ExchangeExtractor\Rule;


use Application\Exception\IncorrectInputData;

class ArraySimpleRule implements ExchangeRule
{

    public static function extract($string)
    {
        // e.x. przelicz (“GBP 4000”, “PLN 350”) na (“EUR”, “GBP”)
        // e.x. convert ("GBP 4000", "PLN 350") to ("EUR", "GBP")
        // e.x. convert ('GBP 4000', 'PLN 350') to ('EUR', 'GBP')
        if (preg_match('/\s*([\p{L}\s]+)\s+(\(([\“|\"|\']\p{L}+\s+[0-9\.]+[\”|\"|\']\,*\ *)+\))\s+([\p{L}\s]+)\s+(\(([\“|\"|\']\p{L}+[\”|\"|\']\,*\ *)+\))\s*/u', $string, $matches)) {

            $realInput = [];
            $realOutput = [];

            // extract data from the first array
            preg_match_all('/[\“|\"|\']\p{L}* [0-9\.]*[\”|\"|\']/u', $matches[2], $inputs);
            foreach ($inputs[0] as $input) {
                preg_match('/(\p{L}+)/u', $input, $inputText);
                preg_match('/([0-9\.]+)/u', $input, $inputAmount);
                array_push($realInput, [
                    'currency' => $inputText[1],
                    'amount' => $inputAmount[1]
                ]);
            }

            // extract data from the second array
            preg_match_all('/[\“|\"|\']\p{L}*[\”|\"|\']/u', $matches[5], $outputs);
            foreach ($outputs[0] as $output) {
                preg_match('/(\p{L}+)/u', $output, $outputText);
                array_push($realOutput, [
                    'currency' => $outputText[1],
                ]);
            }

            // check size of the arrays
            if (count($realInput) !== count($realOutput)) {
                throw new IncorrectInputData();
            }

            // form the final array
            $final = [];
            foreach ($realInput as $index => $input) {
                array_push($final, [
                    'initialCurrency' => $input['currency'],
                    'amount' => $input['amount'],
                    'secondCurrency' => $realOutput[$index]['currency']
                ]);
            }

            return $final;
        }
        return null;
    }

}