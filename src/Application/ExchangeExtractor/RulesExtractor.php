<?php

namespace Application\ExchangeExtractor;

use Application\Exception\CurrencyPairNotSupported;
use Application\Exception\IncorrectInputData;
use Application\ExchangeExtractor\Rule\ArraySimpleRule;
use Application\ExchangeExtractor\Rule\NaturalGuessRule;
use Application\ExchangeExtractor\Rule\NaturalSimpleRule;
use Application\ExchangeExtractor\Rule\SimpleRule;
use Application\ExchangeExtractor\Rule\SimpleRuleReversed;

class RulesExtractor implements ExchangeExtractorProvider
{

    public function getDataFromString($string)
    {
        $results = ArraySimpleRule::extract($string);
        if ($results) {
            return $this->checkData($results);
        }

        $results = SimpleRule::extract($string);
        if ($results) {
            return $this->checkData($results);
        }

        $results = SimpleRuleReversed::extract($string);
        if ($results) {
            return $this->checkData($results);
        }

        $results = NaturalSimpleRule::extract($string);
        if ($results) {
            return $this->checkData($results);
        }

        $results = NaturalGuessRule::extract($string);
        if ($results) {
            return $this->checkData($results);
        }

        throw new IncorrectInputData();
    }


    public function normalize($currency)
    {
        $currency = mb_strtolower($currency);
        if (!$this->in_array_r($currency, ExchangeExtractorProvider::CURRENCIES_EXT)) {
            throw new CurrencyPairNotSupported();
        } else {
            $currency = $this->search_array_r($currency) ? $this->search_array_r($currency) : $currency;
        }

        return $currency;
    }

    public function checkData($results)
    {
        foreach ($results as &$result) {
            if (!$this->in_array_r(mb_strtolower($result['initialCurrency']), ExchangeExtractorProvider::CURRENCIES_EXT)) {
                throw new CurrencyPairNotSupported();
            } else {
                $result['initialCurrency'] = $this->search_array_r(mb_strtolower($result['initialCurrency'])) ? $this->search_array_r(mb_strtolower($result['initialCurrency'])) : strtolower($result['initialCurrency']);
            }

            if (!$this->in_array_r(mb_strtolower($result['secondCurrency']), ExchangeExtractorProvider::CURRENCIES_EXT)) {
                throw new CurrencyPairNotSupported();
            } else {
                $result['secondCurrency'] = $this->search_array_r(mb_strtolower($result['secondCurrency'])) ? $this->search_array_r(mb_strtolower($result['secondCurrency'])) : strtolower($result['secondCurrency']);
            }

            if (!is_numeric($result['amount']) || $result['amount'] <= 0) {
                throw new IncorrectInputData();
            }

            $result['initialCurrencyShort'] = ExchangeExtractorProvider::CURRENCIES_EXT[mb_strtolower($result['initialCurrency'])][0];
            $result['secondCurrencyShort'] = ExchangeExtractorProvider::CURRENCIES_EXT[mb_strtolower($result['secondCurrency'])][0];
            $result['initialCurrencyLong'] = ExchangeExtractorProvider::CURRENCIES_EXT[mb_strtolower($result['initialCurrency'])][2];
            $result['secondCurrencyLong'] = ExchangeExtractorProvider::CURRENCIES_EXT[mb_strtolower($result['secondCurrency'])][2];

        }

        return $results;
    }

    /*
     * https://stackoverflow.com/questions/4128323/in-array-and-multidimensional-array
     */
    protected function in_array_r($needle, $haystack, $strict = false)
    {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }

    private function search_array_r($needle, $array = ExchangeExtractorProvider::CURRENCIES_EXT)
    {
        $key = null;
        array_walk($array, function ($v, $k) use (&$key, $needle) {
            if (in_array($needle, $v['1'])) {
                $key = $k;
            }
        });

        return $key;
    }

}