<?php

namespace Application\ExchangeExtractor;

use Application\Exception\CurrencyPairNotSupported;
use Application\Exception\IncorrectInputData;
use PHPUnit\Framework\TestCase;

class RulesExtractorTest extends TestCase
{
    public function testGetDataFromStringCorrect()
    {
        $rulesExtractor = new RulesExtractor();

        $string = "convert 50 pln to eur";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('pln', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(50, $results[0]['amount']);

        $string = "how much euro is 10050 pounds";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(10050, $results[0]['amount']);

        $string = "ile to złotówek to 50 euro";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('eur', $results[0]['initialCurrency']);
        $this->assertEquals('pln', $results[0]['secondCurrency']);
        $this->assertEquals(50, $results[0]['amount']);

        $string = "convert GBP 10050 to EUR";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(10050, $results[0]['amount']);

        $string = "convert (“GBP 4000”, “PLN 350”) to (“EUR”, “GBP”)";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(4000, $results[0]['amount']);
        $this->assertEquals('pln', $results[1]['initialCurrency']);
        $this->assertEquals('gbp', $results[1]['secondCurrency']);
        $this->assertEquals(350, $results[1]['amount']);

        $string = "convert ('GBP 4000', 'PLN 350') to ('EUR', 'GBP')";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(4000, $results[0]['amount']);
        $this->assertEquals('pln', $results[1]['initialCurrency']);
        $this->assertEquals('gbp', $results[1]['secondCurrency']);
        $this->assertEquals(350, $results[1]['amount']);

        $string = "convert (\"GBP 4000\", \"PLN 350\") to (\"EUR\", \"GBP\")";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(4000, $results[0]['amount']);
        $this->assertEquals('pln', $results[1]['initialCurrency']);
        $this->assertEquals('gbp', $results[1]['secondCurrency']);
        $this->assertEquals(350, $results[1]['amount']);

        $string = "przelicz GBP 10050 na EUR";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(10050, $results[0]['amount']);

        $string = "przelicz (“GBP 4000”, “PLN 350”) na (“EUR”, “GBP”)";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(4000, $results[0]['amount']);
        $this->assertEquals('pln', $results[1]['initialCurrency']);
        $this->assertEquals('gbp', $results[1]['secondCurrency']);
        $this->assertEquals(350, $results[1]['amount']);

        $string = "przelicz ('GBP 4000', 'PLN 350') na ('EUR', 'GBP')";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(4000, $results[0]['amount']);
        $this->assertEquals('pln', $results[1]['initialCurrency']);
        $this->assertEquals('gbp', $results[1]['secondCurrency']);
        $this->assertEquals(350, $results[1]['amount']);

        $string = "przelicz (\"GBP 4000\", \"PLN 350\") na (\"EUR\", \"GBP\")";

        $results = $rulesExtractor->getDataFromString($string);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(4000, $results[0]['amount']);
        $this->assertEquals('pln', $results[1]['initialCurrency']);
        $this->assertEquals('gbp', $results[1]['secondCurrency']);
        $this->assertEquals(350, $results[1]['amount']);
    }

    public function testWrongCurrency()
    {
        $rulesExtractor = new RulesExtractor();

        $data = [
            'initialCurrency' => 'yyy',
            'secondCurrency' => 'zzz',
            'amount' => 50,
        ];

        $this->expectException(CurrencyPairNotSupported::class);
        $rulesExtractor->checkData([$data]);

        $data = [
            'initialCurrency' => 'eur',
            'secondCurrency' => 'gbp',
            'amount' => 50,
        ];

        $this->expectException(CurrencyPairNotSupported::class);
        $rulesExtractor->checkData([$data]);

        $data = [
            'initialCurrency' => 'pln',
            'secondCurrency' => 'eur',
            'amount' => 0,
        ];

        $this->expectException(IncorrectInputData::class);
        $rulesExtractor->checkData([$data]);
    }

    public function testCheckNormalDataCorrect()
    {

        $rulesExtractor = new RulesExtractor();

        $data = [
            'initialCurrency' => 'pln',
            'secondCurrency' => 'eur',
            'amount' => 50,
        ];

        $results = $rulesExtractor->checkData([$data]);

        $this->assertEquals('pln', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(50, $results[0]['amount']);
    }

    public function testCheckDifferentDataCorrect()
    {

        $rulesExtractor = new RulesExtractor();

        $data = [
            'initialCurrency' => 'zloty',
            'secondCurrency' => 'euros',
            'amount' => 50,
        ];

        $results = $rulesExtractor->checkData([$data]);

        $this->assertEquals('pln', $results[0]['initialCurrency']);
        $this->assertEquals('eur', $results[0]['secondCurrency']);
        $this->assertEquals(50, $results[0]['amount']);

        $data = [
            'initialCurrency' => 'pounds',
            'secondCurrency' => 'zł',
            'amount' => 50,
        ];

        $results = $rulesExtractor->checkData([$data]);

        $this->assertEquals('gbp', $results[0]['initialCurrency']);
        $this->assertEquals('pln', $results[0]['secondCurrency']);
        $this->assertEquals(50, $results[0]['amount']);
    }
}