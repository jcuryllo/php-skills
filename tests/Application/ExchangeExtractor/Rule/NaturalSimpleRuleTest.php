<?php

namespace Tests\Application\ExchangeExtractor\Rule;

use Application\ExchangeExtractor\Rule\NaturalSimpleRule;
use Application\ExchangeExtractor\RulesExtractor;
use PHPUnit\Framework\TestCase;

class NaturalSimpleRuleTest extends TestCase
{

    public function testCorrectExtract()
    {
        $rulesExtractor = new RulesExtractor();

        $string = "how much is 121 eur in pln";

        $results = NaturalSimpleRule::extract($string);
        $this->assertEquals(121, $results[0]['amount']);
        $this->assertEquals('eur', $rulesExtractor->checkData($results)[0]['initialCurrency']);
        $this->assertEquals('pln', $rulesExtractor->checkData($results)[0]['secondCurrency']);

        $string = "ile to jest 156 euro w złotówkach";

        $results = NaturalSimpleRule::extract($string);
        $this->assertEquals(156, $results[0]['amount']);
        $this->assertEquals('eur', $rulesExtractor->checkData($results)[0]['initialCurrency']);
        $this->assertEquals('pln', $rulesExtractor->checkData($results)[0]['secondCurrency']);

        $string = "cuando es 123 euros en zloty";

        $results = NaturalSimpleRule::extract($string);
        $this->assertEquals(123, $results[0]['amount']);
        $this->assertEquals('eur', $rulesExtractor->checkData($results)[0]['initialCurrency']);
        $this->assertEquals('pln', $rulesExtractor->checkData($results)[0]['secondCurrency']);
    }
}

