<?php

namespace Tests\Application\ExchangeExtractor\Rule;

use Application\ExchangeExtractor\Rule\NaturalGuessRule;
use Application\ExchangeExtractor\RulesExtractor;
use PHPUnit\Framework\TestCase;

class NaturalGuessRuleTest extends TestCase
{

    public function testCorrectExtract()
    {
        $rulesExtractor = new RulesExtractor();

        $string = "how much euro is 10050 pounds";

        $results = NaturalGuessRule::extract($string);
        $this->assertEquals(10050, $rulesExtractor->checkData($results)[0]['amount']);
        $this->assertEquals('gbp', $rulesExtractor->checkData($results)[0]['initialCurrency']);
        $this->assertEquals('eur', $rulesExtractor->checkData($results)[0]['secondCurrency']);

        $string = "how many zloty is 20.50 euro";

        $results = NaturalGuessRule::extract($string);
        $this->assertEquals(20.50, $rulesExtractor->checkData($results)[0]['amount']);
        $this->assertEquals('eur', $rulesExtractor->checkData($results)[0]['initialCurrency']);
        $this->assertEquals('pln', $rulesExtractor->checkData($results)[0]['secondCurrency']);
    }
}

