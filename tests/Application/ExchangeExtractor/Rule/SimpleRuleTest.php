<?php

namespace Tests\Application\ExchangeExtractor\Rule;

use Application\ExchangeExtractor\Rule\SimpleRule;
use PHPUnit\Framework\TestCase;

class SimpleRuleTest extends TestCase
{
    public function testCorrectExtract()
    {
        $stringsCorrect = [
            "convert eur 500 to pln",
            "przelicz eur 500 na pln",
            "convertir eur 500 a pln"
        ];

        foreach ($stringsCorrect as $string) {
            $results = SimpleRule::extract($string);
            $this->assertEquals(500, $results[0]['amount']);
            $this->assertEquals('eur', $results[0]['initialCurrency']);
            $this->assertEquals('pln', $results[0]['secondCurrency']);
        }
    }
}

