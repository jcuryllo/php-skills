<?php

namespace Tests\Application\ExchangeExtractor\Rule;

use Application\ExchangeExtractor\Rule\ArraySimpleRule;
use PHPUnit\Framework\TestCase;

class ArraySimpleRuleTest extends TestCase
{

    public function testCorrectExtract()
    {
        $stringsCorrect = [
            "przelicz (“GBP 4000”, “PLN 350”) na (“EUR”, “GBP”)",
            "convert (\"GBP 4000\", \"PLN 350\") to (\"EUR\", \"GBP\")",
            "convert ('GBP 4000', 'PLN 350') to ('EUR', 'GBP')",
        ];

        foreach ($stringsCorrect as $string) {
            $results = ArraySimpleRule::extract($string);

            $this->assertEquals(4000, $results[0]['amount']);
            $this->assertEquals('gbp', mb_strtolower($results[0]['initialCurrency']));
            $this->assertEquals('eur', mb_strtolower($results[0]['secondCurrency']));

            $this->assertEquals(350, mb_strtolower($results[1]['amount']));
            $this->assertEquals('pln', mb_strtolower($results[1]['initialCurrency']));
            $this->assertEquals('gbp', mb_strtolower($results[1]['secondCurrency']));
        }
    }
}

