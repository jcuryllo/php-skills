<?php

namespace Tests\Application\ExchangeExtractor\Rule;

use Application\ExchangeExtractor\Rule\SimpleRuleReversed;
use PHPUnit\Framework\TestCase;

class SimpleRuleReversedTest extends TestCase
{

    public function testCorrectExtract()
    {
        $stringsCorrect = [
            "convert 500 eur to pln",
            "convertir 500 eur a pln",
            "przelicz 500 eur na pln",
        ];

        foreach ($stringsCorrect as $string) {
            $results = SimpleRuleReversed::extract($string);
            $this->assertEquals(500, $results[0]['amount']);
            $this->assertEquals('eur', $results[0]['initialCurrency']);
            $this->assertEquals('pln', $results[0]['secondCurrency']);
        }
    }
}

